#include <iostream>
#include <string>
#include <time.h>
#include "gun.h"
using namespace std;

int checkGunStatus(gun x, int t, int zd);

int main(void)
{
	srand((unsigned)time(0));
	int zombieCounter = 0;
	int time = 0;
	int zombieDistance = 0;
	int random = rand()%4;
	int checkStore;
	float shotsFired = 0;
	float shotsHit = 0;
	float shotsMissed = 0;
	int zombiesKilled = 0;
	gun gun1(0/*random*/);
	
	//random = rand()%4 + 1;
	gun gun2(1/*random*/);
	
	//random = rand()%4 + 1;
	gun gun3(2/*random*/);
	
	//random = rand()%4 + 1;
	gun gun4(3/*random*/);


	cout << "Created a " << gun1.getName() << " and " << gun2.getName() << " and " << gun3.getName() << " and " << gun4.getName() << "\n\n\n";
	
	
	do
	{
		//check the world time and create new zombies at 100 distance every 100 time steps
		if (time % 100 == 0)
		{
			cout << "Creating New Zombies\n\n\n";
			if (zombieCounter == 0)//only reset if all zombies were killed last round
			{
				zombieCounter = 100;
				zombieDistance = 100;
			}
		}
		//increment world time
		time++;

		cout << "Zombies Left " << zombieCounter << "\n\n\n";
		
		//only allow the guns to fire if there are zombies on the field
		if (zombieCounter > 0)
		{
			//stores a value into checkStore depending on the status of the gun
			checkStore = checkGunStatus(gun1, time, zombieDistance);
			
			if (checkStore == 1)//gun shoots and hits the target
			{
				gun1.incrementClip();
				zombieCounter = zombieCounter - 1;//kills one zombie
				zombiesKilled++;
				shotsHit++;
				shotsFired++;
				gun1.incrementTime();
			}
			else if (checkStore == 0)//gun shoots and misses the target
			{
				gun1.incrementClip();
				shotsMissed++;
				shotsFired++;
				gun1.incrementTime();
			}
			else if (checkStore == 3)//gun cant shoot because target is out of range
			{
				gun1.incrementTime();
			}
			else if (checkStore == 2)//gun is set to reload
			{
				cout << "RELOADING\n";
				gun1.setClip(gun1.getClipSize());//resets the clip to the max size
				gun1.setAmmo(gun1.getClipSize());//reduces the ammo by one clip ammount
				gun1.setTime(gun1.getReloadSpeed());//sets the time forward to account for reload time
			}
			cout << "Gun 1 time " << gun1.getTime() - 1 << "\t";
			cout << "clip " << gun1.getClip() << "\t";
			cout << "ammo " << gun1.getAmmo() << "\n\n";

			//sets checkStore to the status for gun 2
			checkStore = checkGunStatus(gun2, time, zombieDistance);

			if (checkStore == 1)//gun shoots and hits the target
			{
				gun2.incrementClip();
				zombieCounter = zombieCounter - 1;//kills one zombie
				zombiesKilled++;
				shotsHit++;
				shotsFired++;
				gun2.incrementTime();
			}
			else if (checkStore == 0)//gun shoots and misses the target
			{
				gun2.incrementClip();
				shotsMissed++;
				shotsFired++;
				gun2.incrementTime();
			}
			else if (checkStore == 3)//gun cant shoot because target is out of range
			{
				gun2.incrementTime();
			}
			else if (checkStore == 2)//gun is set to reload
			{
				cout << "RELOADING\n";
				gun2.setClip(gun2.getClipSize());//reset clip to max clip size
				gun2.setAmmo(gun2.getClipSize());//decrease ammo by one clip size
				gun2.setTime(gun2.getReloadSpeed());//increment the time by the reload speed
			}
			cout << "Gun 2 time " << gun2.getTime() - 1 << "\t";
			cout << "clip " << gun2.getClip() << "\t";
			cout << "ammo " << gun2.getAmmo() << "\n\n";
		
			//sets checkStore to the status of gun 3
			checkStore = checkGunStatus(gun3, time, zombieDistance);

			if (checkStore == 1)//gun shoots and hits the target
			{
				gun3.incrementClip();
				zombieCounter = zombieCounter - 1;//kills a zombie
				zombiesKilled++;
				shotsHit++;
				shotsFired++;
				gun3.incrementTime();
			}
			else if (checkStore == 0)//gun shoots and misses the target
			{
				gun3.incrementClip();
				shotsMissed++;
				shotsFired++;
				gun3.incrementTime();
			}
			else if (checkStore == 3)//gun cant shoot because target is out of range
			{
				gun3.incrementTime();
			}
			else if (checkStore == 2)//gun is set to reload
			{
				cout << "RELOADING\n";
				gun3.setClip(gun3.getClipSize());//set clip back to max clip size
				gun3.setAmmo(gun3.getClipSize());//decrease ammo by one clip
				gun3.setTime(gun3.getReloadSpeed());//increment time up by the reload speed
			}
			cout << "Gun 3 time " << gun3.getTime() - 1 << "\t";
			cout << "clip " << gun3.getClip() << "\t";
			cout << "ammo " << gun3.getAmmo() << "\n\n";

			//sets checkstore to the status of gun 4
			checkStore = checkGunStatus(gun4, time, zombieDistance);

			if (checkStore == 1)//gun shoots and hits the target
			{
				gun4.incrementClip();
				zombieCounter = zombieCounter - 1;//kills one zombie
				zombiesKilled++;
				shotsHit++;
				shotsFired++;
				gun4.incrementTime();
			}
			else if (checkStore == 0)//gun shoots and misses the target
			{
				gun4.incrementClip();
				shotsMissed++;
				shotsFired++;
				gun4.incrementTime();
			}
			else if (checkStore == 3)//gun cant shoot because target is out of range
			{
				gun4.incrementTime();
			}
			else if (checkStore == 2)//gun is set to reload
			{
				cout << "RELOADING\n";
				gun4.setClip(gun4.getClipSize());//sets the clip back to max clip size
				gun4.setAmmo(gun4.getClipSize());//decreases the ammo by one clip size
				gun4.setTime(gun4.getReloadSpeed());//increases the time by the reload speed
			}
			cout << "Gun 4 time " << gun4.getTime() - 1 << "\t";
			cout << "clip " << gun4.getClip() << "\t";
			cout << "ammo " << gun4.getAmmo() << "\n\n";

			zombieDistance--;//zombies get closer by one yard
		}
		else
		{
			//even if the guns cant shoot we still need to increment their time to keep up with the world time
			zombieCounter = 0;
			gun1.incrementTime();
			gun2.incrementTime();
			gun3.incrementTime();
			gun4.incrementTime();
		}


		cout << "world time \t" << time << "\n\n";
		cout << "Zombie Distance \t" << zombieDistance;
		//system("PAUSE");
		system("cls");
	}while(zombieDistance != -1);//keep looping until the zombies reach the survivors

	system("cls");
	cout << "Simulation has ended, survivors were overrun!\n\n";
	cout << "The survivors lasted " << time/60 << " minutes\n";
	cout << "The survivors shot " << shotsFired << " bullets\n";
	cout << "Killing " << zombiesKilled << " zombies\n";
	cout << "With an average accuracy of " << (shotsHit/shotsFired)*100 << "%\n\n";
	system("PAUSE");
}



int checkGunStatus(gun x, int t, int zd)
{
	int r;//random number holder
		if (x.getTime() == t)//check if gun time is equal to world time
		{
			if (x.getClip() != 0)//check to see if the clip is out of ammo
			{
				if (x.getEffectiveRange() >= zd)//see if the zombies are within the effective range of the guns
				{
					r = rand() % 100 +1;//sets a random number to check for hit or miss
					return x.shoot(zd, r);//shoot the gun at the zombies returns 1 for hit 0 for miss
				}
			}
			else
			{
				if (x.getAmmo() != 0)//check to see if the gun is out of ammo
				{
					return 2;//returns 2 to set the gun status to reloading
				}
			}
			return 3;//returns 3 to set gun status to increment time but take no other action
		}
		return 4;//return 4 if the world time and gun time are out of sync. does not increment gun time
}