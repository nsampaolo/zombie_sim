#include <iostream>
using namespace std;
#include "gun.h"

gun::gun(int x)
{
	if (x == 0)
	{
		name = "SPAS-12";
		clipSize = 8;
		clip = 8;
		ammo = 2744;
		accuracy = 31.25;
		reloadSpeed = 9;
		effectiveRange = 25;
		Time = 1;
	}
	else if (x == 1)
	{
		name = "AK-47";
		clipSize = 30;
		clip = 30;
		ammo = 1170;
		accuracy = 60.9375;
		reloadSpeed = 3;
		effectiveRange = 100;//380
		Time = 1;
	}
	else if (x == 2)
	{
		name = "Desert Eagle";
		clipSize = 7;
		clip = 7;
		ammo = 595;
		accuracy = 34.375;
		reloadSpeed = 2;
		effectiveRange = 54;
		Time = 1;
	}
	else if (x == 3)
	{
		name = "Dragonuv";
		clipSize = 10;
		clip = 10;
		ammo = 1490;
		accuracy = 92.8125;
		reloadSpeed = 3;
		effectiveRange = 100;//875
		Time = 1;
	}
}


int gun::shoot(int distance, int random)
{
	float accuracyPerMeter;
	float tempAccuracy;

	accuracyPerMeter = (100 - accuracy) / effectiveRange;
	tempAccuracy = (((effectiveRange - distance) * accuracyPerMeter) + accuracy);


	if (random <= tempAccuracy)
	{
		cout << "The gun Hit\n";
		return 1;
	}
	else
	{
		cout << "The gun Missed\n";
		return 0;
	}
}

int gun::getAmmo()
{
	return ammo;
}

void gun::setAmmo(int x)
{
	ammo = ammo - x;
}

int gun::getClip()
{
	return clip;
}

void gun::setClip(int x)
{
	clip = x;
}

int gun::getEffectiveRange()
{
	return effectiveRange;
}

int gun::getTime()
{
	return Time;
}

string gun::getName()
{
	return name;
}

int gun::getClipSize()
{
	return clipSize;
}

void gun::incrementTime()
{
	Time++;
}

void gun::incrementClip()
{
	clip--;
}

void gun::setTime(int x)
{
	Time = Time + x;
}

int gun::getReloadSpeed()
{
	return reloadSpeed;
}
