#ifndef GUN_H
#define GUN_H

#include <iostream>
using namespace std;

class gun
{
private:
	string name;
	int clipSize;
	int clip;
	int ammo;
	float accuracy;
	int reloadSpeed;
	int effectiveRange;
	int Time;
public:
	gun(int x);
	int getEffectiveRange();
	int getReloadSpeed();

	int getAmmo();
	void setAmmo(int x);

	int getClip();
	void setClip(int x);

	int getClipSize();

	int getTime();
	void setTime(int x);

	string getName();


	void incrementTime();
	void incrementClip();


	int shoot(int distance, int random);
};
#endif